﻿using System;

namespace WexHealth.Core.Presenters.Common
{
    /// <summary>
    /// An exception that occurs when the presenter tries to initialize the view more than once.
    /// </summary>
    public sealed class ViewAlreadyInitializedException : Exception
    {
        public ViewAlreadyInitializedException(string message)
            : base(message)
        { }
    }
}
