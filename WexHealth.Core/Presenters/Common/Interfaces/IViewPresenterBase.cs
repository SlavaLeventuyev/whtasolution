﻿namespace WexHealth.Core.Presenters.Common.Interfaces
{
    public interface IViewPresenterBase
    {
        void InitView(bool isPostBack);
    }
}
